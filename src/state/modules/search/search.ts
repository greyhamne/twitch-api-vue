import {IState} from './search.model'

const state: IState = {
    loading: false,
    payload: {},
    userInput: '',
};

const mutations = {
    'SEARCH_START'(state:IState) {
        state.loading = true;
    },
    'SEARCH_FINISH'(state:IState, payload: {}) {
        state.loading = false;
        state.payload = payload
    }
};

const actions = {
    searchTwitch: ({commit}:any, userInput:string) => {
        commit('SEARCH_START', userInput);
    },
    finishedSearch: ({commit}:any, payload:{}) => {
        commit('SEARCH_FINISH', payload);
    }
}

export default {
    state,
    mutations,
    actions,
}
