import {IState} from './game.model'

const state: IState = {
    searches:[]
};

const mutations = {
    'STORE_SEARCH'(state:IState, payload: any) {
        state.searches.push(payload);
    },
    
};

const actions = {
    storeSearch: ({commit}:any, payload: {}) => {
        commit('STORE_SEARCH', payload);
    }
}

export default {
    state,
    mutations,
    actions,
}
