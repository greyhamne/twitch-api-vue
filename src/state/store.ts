import Vue from 'vue';
import Vuex from 'vuex';

import search from './modules/search/search';
import game from './modules/game/game';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        search,
        game
    }
});
