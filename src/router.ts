import Vue from 'vue';
import Router from 'vue-router';
import Search from './features/search/Search.vue';
import Game from './features/game/Game.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Search,
    },
    {
      path:'/game-information/id/:id/title/:title',
      name: 'game',
      component: Game
    }
  ],
});
