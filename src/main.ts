import Vue from 'vue';
import './plugins/vuetify'
import App from './App.vue';
import router from './router';
import store from './state/store';
import VueRx from 'vue-rx';
import { Observable } from 'rxjs';

Vue.config.productionTip = false;

Vue.use(VueRx, {
  Observable
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
